from setuptools import setup, find_packages

classifiers = [
		"Programming Language :: Python :: 3",
		"License :: OSI Approved :: Apache Software License",
		"Operating System :: OS Independent",
]

setup(
    name = 'SiriusCXRSeg',
    version = '0.0.3',
    author = 'Sirius_nauka16',
    author_email = 'dkhasanov76@gmail.com',
    description = "Проект по сегментации легких.",
    packages=find_packages(),
    package_data = {
        'SiriusCXRSeg': [
            'trained_models'
        ]
    },
    classifiers = classifiers,
    python_requires='>=3.6.9',
    install_requires=['']
)
